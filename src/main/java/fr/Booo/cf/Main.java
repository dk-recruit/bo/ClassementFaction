package fr.Booo.cf;

import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import com.massivecraft.factions.Faction;

import java.util.HashMap;
import java.util.Map;

public class Main extends JavaPlugin implements Listener {
	Map<Faction, Integer> PointFaction = new HashMap<>();
	private FileManager fm = new FileManager(this);
	
	@Override
	public void onEnable() {
		getCommand("classement").setExecutor(new TopCmd(this));
		getCommand("Mtop").setExecutor(new TopManagerCmd(this));

		getServer().getPluginManager().registerEvents(new AllEvent(this), this);

		saveDefaultConfig();

		fm.recupStat();
	}
	
	@Override
	public void onDisable() {
		fm.defineStat();
	}
}
