package fr.Booo.cf;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.massivecraft.factions.Faction;
import com.massivecraft.factions.Factions;

public class TopManagerCmd implements CommandExecutor {
	private Main main;

	TopManagerCmd(Main main) {
		this.main = main;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String arg2, String[] args) {
		if (args.length == 0) {
			mpPlayer(sender);
			return false;
		}

		switch (args[0]) {
			case "add":
				if (args.length == 3) {
					try {
						int point = Integer.parseInt(args[1]);

						Faction fac = Factions.getInstance().getByTag(args[2]);

						if (!main.PointFaction.containsKey(Factions.getInstance().getByTag(args[2]))) main.PointFaction.put(fac, 0);
						int oldPoint = main.PointFaction.get(fac);

						main.PointFaction.remove(fac);
						main.PointFaction.put(fac , point + oldPoint);
					} catch(NumberFormatException e) {
						mpPlayer(sender);
					}
				}
				break;

			case "remove":
				if (args.length == 3) {
					try {
						int point = Integer.parseInt(args[1]);
						Faction fac = Factions.getInstance().getByTag(args[2]);
						if (!main.PointFaction.containsKey(Factions.getInstance().getByTag(args[2]))) main.PointFaction.put(fac, 0);

						int oldPoint = main.PointFaction.get(fac);

						main.PointFaction.remove(fac);
						main.PointFaction.put(fac, oldPoint - point);
					} catch(NumberFormatException e) {
						mpPlayer(sender);
					}
				}
				break;

			case "check":
				Player p = (Player) sender;
				p.sendMessage("&8[&a&lBooo&6&lFo&8]&7 La Faction&2" + args[1] + "&7poss&dent : &2" + main.PointFaction.get(Factions.getInstance().getByTag(args[1])) + "&7points!");
				break;
				
			case "define":
				if (args.length == 3) {
					try {
						int point = Integer.parseInt(args[1]);
						Faction fac = Factions.getInstance().getByTag(args[2]);

						if (!main.PointFaction.containsKey(Factions.getInstance().getByTag(args[2]))) main.PointFaction.put(fac, 0);

						main.PointFaction.remove(fac);
						main.PointFaction.put(fac, point);
					} catch(NumberFormatException e) {
						mpPlayer(sender);
					}
				}
				break;

			default:
				mpPlayer(sender);
		}

		return false;
	}

	private void mpPlayer(CommandSender sender) {
		Player p = (Player) sender;

		p.sendMessage("&7---------------&8[&a&lBooo&6&lFo&8]&7-----------------");
		p.sendMessage("&7Add des Points a la faction : &2/Mtop add &6<Point> &2<faction>");
		p.sendMessage("&7Enlever des Points a la faction : &2/Mtop remove &6<Point> &2<faction>");
		p.sendMessage("&7Définir les Points d'une faction : &2/Mtop define &6<Point> &2<faction>");
		p.sendMessage("&7Check les Points d'une faction : &2/Mtop check &2<faction>");
		p.sendMessage("&7---------------------------------------");
	}
}
