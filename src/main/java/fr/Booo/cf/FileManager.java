package fr.Booo.cf;

import org.bukkit.configuration.file.YamlConfiguration;

import com.massivecraft.factions.Faction;
import com.massivecraft.factions.Factions;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;

class FileManager {
	private Main main;

	FileManager(Main main) {
		this.main = main;
	}

	void recupStat() {
		File file = new File(main.getDataFolder(),"AllPoint.yml");
		YamlConfiguration config = YamlConfiguration.loadConfiguration(file);

		if (!file.exists()) return;

		for (String part : config.getConfigurationSection("point").getKeys(false)) {
			String save = config.getConfigurationSection("point").getString(part + ".nbr");
			
			String[] saves = save.split("\\.");
			main.PointFaction.put(Factions.getInstance().getByTag(saves[0]), Integer.parseInt(saves[1]));
		}
	}

	void defineStat() {
		new HashMap<>(main.PointFaction);

		for (Entry<Faction, Integer> ent : new HashMap<>(main.PointFaction).entrySet()) {

			Faction fac = ent.getKey();

			File file = new File(main.getDataFolder(), "AllPoint.yml");
			YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
			String stock = fac.getTag() + "." + ent.getValue();

			if (file.exists()) file.delete();

			config.set("point." + fac.getId()+".nbr" , stock);

			try {
				config.save(file);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
