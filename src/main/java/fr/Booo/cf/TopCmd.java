package fr.Booo.cf;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.FPlayers;
import com.massivecraft.factions.Faction;
import com.massivecraft.factions.Factions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map.Entry;

public class TopCmd implements CommandExecutor {
	private Main main;

	TopCmd(Main main) {
		this.main = main;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			FPlayer factionPlayer = FPlayers.getInstance().getByPlayer(player);
			Faction yourFaction = factionPlayer.getFaction();

			main.PointFaction.putIfAbsent(yourFaction, 0);

			final List<Entry<Faction, Integer>> entries = new ArrayList<>(main.PointFaction.entrySet());

			entries.sort((e1, e2) -> e2.getValue().compareTo(e1.getValue()));

			player.sendMessage(main.getConfig().getString("messages.top").replace("&", "?"));

			int usedNumber = 10;
			int factionActuelle = 0;
			int placementFaction = main.PointFaction.get(yourFaction) + 1;

			if (args.length == 1) {
				try {
					usedNumber = Integer.parseInt(args[0]);
				} catch (NumberFormatException e) {
					Factions.getInstance().getByTag(args[0]);
					player.sendMessage(Cherchet(Factions.getInstance().getByTag(args[0]), entries.indexOf(Factions.getInstance().getByTag(args[0]))));
					finalInutile(player, yourFaction, placementFaction);
					return false;
				}
			}

			while (factionActuelle < usedNumber) {
				try {
					Faction fac = entries.get(factionActuelle).getKey();

					int pts = entries.get(factionActuelle).getValue();

					factionActuelle++;

					player.sendMessage(main.getConfig().getString("messages.repeat")
									.replace("&", "?")
									.replace("%Placement%", factionActuelle + "")
									.replace("%Faction%", fac.getTag())
									.replace("%point%",  pts + ""));
				} catch (IndexOutOfBoundsException e) {
					finalInutile(player , yourFaction , placementFaction);
					return false;
				}
			}

			finalInutile(player, yourFaction, placementFaction);
		}

		return false;
	}

	private String Cherchet(Faction fac, int Pla) {
		int pts = main.PointFaction.get(fac);

		return main.getConfig().getString("messages.Spec")
						.replace("&", "?")
						.replace("%Placement%", Pla + "")
						.replace("%Faction%", fac.getTag())
						.replace("%point%",  pts + "");
	}

	private void finalInutile(Player p, Faction Yfac, int bli) {
		p.sendMessage(main.getConfig().getString("messages.you").replace("%YourFaction%", Yfac.getTag())
						.replace("&", "?")
						.replace("%point%" , main.PointFaction.get(Yfac) + "")
						.replace("%Placement%" , bli + ""));

		p.sendMessage(main.getConfig().getString("messages.next").replace( "&", "?"));
		p.sendMessage(main.getConfig().getString("messages.foot").replace( "&", "?"));
	}
}	
