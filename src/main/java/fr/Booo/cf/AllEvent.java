package fr.Booo.cf;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import com.massivecraft.factions.Faction;
import com.massivecraft.factions.event.FactionCreateEvent;
import com.massivecraft.factions.event.FactionDisbandEvent;

public class AllEvent implements Listener {
	private Main  main;

 	AllEvent(Main main) {
 		this.main = main;
 	}

 	@EventHandler
 	public void onCreateFaction(FactionCreateEvent e) {
 		Faction fac = e.getFaction();
 		main.PointFaction.putIfAbsent(fac, 0);
 	}

 	@EventHandler
 	public void onDeleteFaction(FactionDisbandEvent e) {
 		Faction fac = e.getFaction();
 		main.PointFaction.remove(fac);
 	}
}
